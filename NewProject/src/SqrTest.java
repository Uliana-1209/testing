import System;
import System.Text;
import System.Linq;
import System.Collection.Generic;
import AutoItX3.lib;

    public class SqrTest extends TestBase
    {
        public void SQRtest() {
        	
            OperationData items = new OperationData()
            {
                Third = 5,
                Sign = "SQR"
            }
            ;

            app.OperationHelper.EnterNumber(items.Third);
    		app.OperationHelper.EnterNumber(items.Sign);
    		app.OperationHelper.Result();
    		
    		String result = app.OperationHelper.GetResult();
    		String shouldBe = app.OperationHelper.Calculate(items);

    		Assert.AraEqual(result, shouldBe);
    	}
    }
}