import System;
import System.Text;
import System.Linq;
import System.Collection.Generic;
import AutoItX3.lib;



public class AppManager<aux> {
	
	
	private static AutoItX3 aux;
	private static OperationHelper oper;
	static String WinTITLE = "Калькулятор";
	
	
	public void AppManager()  {
		aux = new AutoItX3();
		aux.Run("C:\Windows\System32\calc.exe");
		oper = new OperationHelper(this);
		aux.WinWait(WinTITLE);
		aux.WinActivate(WinTITLE);
		aux.WinWaitActive(WinTITLE);
				
	}

	public  void Stop() {
		// TODO Auto-generated method stub
		aux.WinClose(WinTITLE);
	} 
	public  Object getAutoItX3() {
		return  aux;
		}
	
	public  OperationHelper getOperation(){
		return  oper;
		}

}
