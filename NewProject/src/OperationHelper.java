import System;
import System.Text;
import System.Linq;
import System.Collection.Generic;
import AutoItX3.lib;
import org.omg.CORBA.INTERNAL;


public class OperationHelper extends HelperBase {
	
	
	public OperationHelper(AppManager manager) {
		super(manager);
		// TODO Auto-generated constructor stub
	}

	public void EnterNumber(int number) 
		{ 
		switch (number) 
		{ 
			case 0: 
				aux.ControlClick(WinTITLE, "", "Button6"); 
				break; 
			case 1: 
				aux.ControlClick(WinTITLE, "", "Button22"); 
				break; 
			case 2: 
				aux.ControlClick(WinTITLE, "", "Button11"); 
				break; 
			case 3: 
				aux.ControlClick(WinTITLE, "", "Button16"); 
				break; 
			case 4: 
				aux.ControlClick(WinTITLE, "", "Button4"); 
				break; 
			case 5: 
				aux.ControlClick(WinTITLE, "", "Button10"); 
				break; 
			case 6: 
				aux.ControlClick(WinTITLE, "", "Button15"); 
				break; 
			case 7: 
				aux.ControlClick(WinTITLE, "", "Button3"); 
				break; 
			case 8: 
				aux.ControlClick(WinTITLE, "", "Button9"); 
				break; 
			case 9: 
				aux.ControlClick(WinTITLE, "", "Button14"); 
				break; 
			default: 
			break; 
		} 
		} 

	public void EnterSign(String sign) 
	{ 
		switch (sign) 
		{ 
		case "+": 
			aux.ControlClick(WinTITLE, "", "Button23"); 
			break; 
		case "-": 
			aux.ControlClick(WinTITLE, "", "Button22"); 
			break; 
		case "SQR": 
			aux.ControlClick(WinTITLE, "", "Button25"); 
			break; 
		default: 
			break; 
		} 
		
	public void Result (int p){
		aux.ControlClick(WinTITLE, "", "Button28"); 
	}
	
	INTERNAL  GetResult (int p){
		return aux.WinGetText(WinTITLE).Replace("\n", ""); 
	}
	
	
	public void Calculate(OperationData items) {
		int result;
			switch (items.Sign){
				case "+";
					result = items.First + items.Second;
					break;
				case "-";
					result = items.First - items.Second;
					break;
				case "*";
				result = items.First * items.Second;
				break;	
				case "SQR":
                    result = Math.Sqr(items.Third);
                    break;
				default;
					result = 0;
					break;
			}
			return result.toString();
			
	}


