

public class SimpleTest extends TestBase {
	public void AddItionTest(){
		OperationData items = new OperationData()
		{
			first =5;
			sign= "+";
			second = 3;
		}
		app.OperationHelper.EnterNumber(items.First);
		app.OperationHelper.EnterNumber(items.Sign);
		app.OperationHelper.EnterNumber(items.Second);
		app.OperationHelper.Result();
		
		String result = app.OperationHelper.GetResult();
		String shouldBe = app.OperationHelper.Calculate(items);

		Assert.AraEqual(result, shouldBe);
	}
}
