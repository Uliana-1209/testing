import System;
import System.Text;
import System.Linq;
import System.Collection.Generic;
import AutoItX3.lib;


public class TestBase {
	protected AppManager app;
	
	public void InitTests () {
		app = new AppManager();
	}
	public void StopTests(){
		app.Stop();
	}
	}
