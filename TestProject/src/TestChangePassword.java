import java.util.List;

import org.junit.Assert;
import org.testng.annotations.DataProvider;

import com.sun.org.apache.xml.internal.serialize.XMLSerializer;


public class TestChangePassword extends TestBase {
	
	@DataProvider ( name = "DataSource") 
	public static Iterable<ChangePassword> ChangePasswordFromXmlFile() {
		return (List<ChangePassword>) new XMLSerializer (typeof (List<ChangePassword>) DeSerializer(new SteamReader(@"Data.xml"));
	}
		public void addNewPassword(){
			app.navigationHelper.gotoSettingsPage();
			app.changeHelper.ChangePassword();
			ChangePassword chpassword = new ChangePassword(generateRandomString(15), generateRandomString(15)); 
			app.changeHelper.ChangeForm();
	
			Assert.assertTrue(app.changeHelper.IsChangePaswordIn());
			
		}

}