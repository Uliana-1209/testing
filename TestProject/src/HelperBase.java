import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class HelperBase {
	
	  protected WebDriver driver;
	  protected AppManager manager;
	  public boolean acceptNextAlert = true;
	  
	  
	  public HelperBase (AppManager manager) {
		  this.manager = manager;
		  driver = manager.driver;
	  }
	  
      public void FillData (String value, String type){
    	  WebElement element = driver.findElement(By.id(type)); 
    	  element.sendKeys(toString());
      }
            
      public void PressButton (String value){
    	  driver.findElement(By.linkText(value)).click();
      }
      
	  protected boolean isElementPresent(By by) {
		    try {
		      driver.findElement(by);
		      return true;
		    } catch (NoSuchElementException e) {
		      return false;
		    }
		  }
	  

		  private boolean isAlertPresent() {
		    try {
		      driver.switchTo().alert();
		      return true;
		    } catch (NoAlertPresentException e) {
		      return false;
		    }
		  }
		  

		  private String closeAlertAndGetItsText() {
		    try {
		      Alert alert = driver.switchTo().alert();
		      String alertText = alert.getText();
		      if (acceptNextAlert) {
		        alert.accept();
		      } else {
		        alert.dismiss();
		      }
		      return alertText;
		    } finally {
		      acceptNextAlert = true;
		    }}
		  
		}

