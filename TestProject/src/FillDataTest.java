
import java.util.List;

import org.junit.Assert;
import org.testng.annotations.DataProvider;

import com.sun.org.apache.xml.internal.serialize.XMLSerializer;


public class FillDataTest extends TestBase {
	
	@DataProvider ( name = "DataSource") 
	public static Iterable<FillData> FillDataFromXmlFile() {
		return (List<FillData>) new XMLSerializer (typeof (List<FillData>) DeSerializer(new SteamReader(@"Data.xml"));
	}
		public void addPersonalData(){
			app.navigationHelper.gotoProfilePage();
			app.dataHelper.FillData();
			FillData surname = new FillData ( generateRandomString(15), generateRandomString(15), generateRandomString(15));
			//FillData surname = new FillData ( "patronymic", " ", " ");
			app.dataHelper.SaveDataForm();
			
			Assert.assertTrue(app.dataHelper.IsDataIn());
			
		}

}
