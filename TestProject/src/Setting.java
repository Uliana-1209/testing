
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.xml.XmlInclude;
import org.xml.*;
import javax.xml.bind.annotation.XmlAnyElement;
import processing.xml.XMLElement;
import java.io.Serializable;
import java.io.*;
 
public class Setting 
	{ 
	
	public static String file = "Setting.xml"; 
	public static String baseURL; 
	public static String login; 
	public static String password; 
	private static String user; 
	private static XMLElement document; 
	
 public  Setting() 
 	{ 
	 if (!System.io.File.Exists(file)) 
	 { 
		 throw new Exception("Problem"); 
		 document = new XMLElement(); 
	 } 
 	} 

 public String getBaseURL()
 	{  
	 if (baseURL == null) 
	 { 
		 XMLNode node = document.DocumentElement.SelectSingleNode("baseURL"); 
		 baseURL = node.InnerText; 
	 } 
	 return baseURL;

 	}  


 public  String getLogin () 
 	{ 

	 if (login == null) 
	 { 
		 XmlNode node = document.DocumentElement.SelectSingleNode("login"); 
		 login = node.InnerText; 
	 } 
	 return login; 

 	} 
 
 public String getPassword ()
 	{ 

	 if (password == null) 
	 { 
		 XmlNode node = document.DocumentElement.SelectSingleNode("password"); 
		 password = node.InnerText; 
	 } 
	 return password; 

 	} 
 public String getUser()  
    {
	 if (user == null) 
	 { 
		 XMLNode node = document.DocumentElement.SelectSingleNode("user"); 
		 user = node.InnerText; 
	 } 
	 return user;  
	} 

} 
	