import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AppManager {
	
	public final String LoginHelper = null;
	public String baseUrl;
	protected WebDriver driver;
	protected LoginHelper loginHelper;
	protected NavigationHelper navigationHelper;
	protected DataHelper dataHelper;
	protected ChangeHelper changeHelper;
	public MapHelper mapHelper;
	
	
	private static ThreadLocal <AppManager> app = new ThreadLocal <AppManager> ();
	
    public AppManager() {
    	driver = new FirefoxDriver();
    	baseUrl = Setting.baseURL;
    	loginHelper = new LoginHelper(null);
    	navigationHelper = new NavigationHelper(null);
    	dataHelper = new DataHelper(null);
    	changeHelper = new ChangeHelper(null);
    	mapHelper = new MapHelper(null);
    }
    
    public static AppManager GetInstance () {
    	if (!app.IsValueCreated)
    	{
    		AppManager newInstance = new AppManager();
    		newInstance.navigationHelper.GotoHomePage();
    		app.Value = newInstance;
    		
    	}
    	   return app.Value;
    } 
    
    public WebDriver getWebDriver() {
        return driver;
       }
    	
    public LoginHelper getLoginHelper() {
            return loginHelper;
           }
    public NavigationHelper getNavigationHelper() {
        return navigationHelper;
       }
    public DataHelper getDataHelper() {
        return dataHelper;
       }	
    public MapHelper getMapHelper() {
        return mapHelper;
       }
    }
    
    
    

