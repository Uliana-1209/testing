import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;


public class LoginTest extends TestBase {
	
	
	public void LoginWithValidCreditionals() {
		
		
		AccountData userdata = new AccountData("uliana-1209@rambler.ru", "uliana-1209"); 
		app.loginHelper.Login(userdata);
		app.loginHelper.Logout();
		
		Assert.assertTrue(app.loginHelper.IsLoggedIn());
		
	}

}
