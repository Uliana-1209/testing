
import java.util.List;

import org.junit.Assert;
import org.testng.annotations.DataProvider;

import com.sun.org.apache.xml.internal.serialize.XMLSerializer;


public class MapMegaTest extends TestBase {
	
	@DataProvider ( name = "DataSource") 
	
	public static Iterable<MapMega> MapMegaFromXmlFile() {
		return (List<MapMega>) new XMLSerializer (typeof (List<MapMega>) DeSerializer(new SteamReader(@"Data.xml"));
	}
	
		public void addPersonalData(){
			app.navigationHelper.gotoMapMegaPage();
			app.mapHelper.MapMega();
			MapMega information = new MapMega ( generateRandomString(20));
			app.mapHelper.InformationAboutShop();
			
			Assert.assertTrue(app.mapHelper.IsMapMegaIn());
			
		}

}